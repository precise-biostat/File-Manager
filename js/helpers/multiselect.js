/*
 * Copyright (c) 2015 Samsung Electronics Co., Ltd. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*global define*/

/**
 * Selecting list elements helper module.
 *
 * @module helpers/multiselect
 * @requires {@link core/template}
 * @namespace helpers/multiselect
 */
define('helpers/multiselect',
    ['core/template'],
    function multiSelectModule(template) {
        /*jshint validthis:true */
        /*jslint nomen: true*/
        'use strict';

        /**
         * Item selected CSS class string.
         *
         * @memberof helpers/multiselect
         * @private
         * @const {string}
         */
        var ITEM_SELECTED_CLASS = 'item-selected',

            /**
             * Open multiselect CSS class name.
             *
             * It allows the application to decide
             * whether the multiselect menu should be visible or not.
             *
             * @memberof helpers/multiselect
             * @private
             * @const {string}
             */
            OPEN_MULTISELECT_CLASS = 'open',

            /**
             * Show button CSS class name.
             *
             * @memberof helpers/multiselect
             * @private
             * @const {string}
             */
            SHOW_BTN_CLASS = 'show-btn';

        /**
         * Creates helper DOM elements, appends them to the DOM tree
         * and returns them.
         *
         * @memberof helpers/multiselect
         * @private
         * @param {HTMLElement} listElement
         * @returns {object}
         */
        function createDOMElements(listElement) {
            var parent = listElement.parentNode,
                wrapper = template.getElement('multiselect-elements');

            parent.appendChild(wrapper);

            return {
                wrapper: wrapper,
                handlerBtn: wrapper.querySelector('.handler-btn'),
                handlerBtnText: wrapper.querySelector('.handler-btn-text'),
                selectAllBtn: wrapper.querySelector('.select-all-btn'),
                deselectAllBtn: wrapper.querySelector('.deselect-all-btn'),
                actionBtn: wrapper.querySelector('.action-btn'),
                actionBtnText: wrapper.querySelector('.action-btn-text')
            };
        }

        /**
         * Finds and returns the closest parent element of the specified element
         * having the specified tag name.
         *
         * @memberof helpers/multiselect
         * @private
         * @param {HTMLElement} element
         * @param {string} tagName
         * @returns {HTMLElement}
         */
        function findDOMParent(element, tagName) {
            while (element.parentNode.tagName !== tagName &&
            element.parentNode.tagName !== 'BODY') {
                element = element.parentNode;
            }
            return element;
        }

        /**
         * Handles click events on the list.
         *
         * @memberof helpers/multiselect
         * @private
         * @param {Event} event
         */
        function onListClick(event) {
            var target = findDOMParent(event.target, 'LI');

            if (!target.classList.contains(ITEM_SELECTED_CLASS)) {
                target.classList.add(ITEM_SELECTED_CLASS);
                this._selectCount += 1;
                this._modeShow();
            } else {
                target.classList.remove(ITEM_SELECTED_CLASS);
                this._selectCount -= 1;
                if (this._selectCount <= 0) {
                    this._modeHide();
                } else {
                    this._refreshText();
                }
            }
        }

        /**
         * Handles click events on the handler button.
         *
         * @memberof helpers/multiselect
         * @private
         */
        function onHandlerClick() {
            this._domElements.wrapper.classList.add(OPEN_MULTISELECT_CLASS);
        }

        /**
         * Returns an array of HTML list items having the 'item-selected' class
         * set.
         *
         * @memberof helpers/multiselect
         * @private
         * @param {NodeList} listItems
         * @returns {HTMLElement[]}
         */
        function filterSelected(listItems) {
            var i = 0,
                length = listItems.length,
                item = null,
                result = [];

            for (; i < length; i += 1) {
                item = listItems[i];
                if (item.classList.contains(ITEM_SELECTED_CLASS)) {
                    result.push(item);
                }
            }

            return result;
        }

        /**
         * Handles click events on the action button. Executes action handler
         * set by the setActionHandler method.
         *
         * @memberof helpers/multiselect
         * @private
         */
        function onActionExecute() {
            var listElements = filterSelected(this._listItems),
                handler = this._actionHandler;

            if (typeof handler === 'function') {
                handler(listElements);
            }
        }

        /**
         * Handles click events on the select all button.
         *
         * @memberof helpers/multiselect
         * @private
         */
        function onSelectAllClick() {
            var list = this._listItems,
                len = list.length,
                i = 0;

            for (; i < len; i += 1) {
                list[i].classList.add(ITEM_SELECTED_CLASS);
            }

            this._selectCount = len;
            this._modeShow();
        }

        /**
         * Handles click events on the deselect all button.
         *
         * @memberof helpers/multiselect
         * @private
         */
        function onDeselectAllClick() {
            var list = this._listItems,
                len = list.length,
                i = 0;

            for (; i < len; i += 1) {
                list[i].classList.remove(ITEM_SELECTED_CLASS);
            }

            this._selectCount = 0;
            this._modeHide();
        }

        /**
         * Registers event handlers.
         *
         * @memberof helpers/multiselect
         * @private
         */
        function bindEvents() {
            this._listElement.addEventListener('click', this._onListClick);
            this._domElements.handlerBtn.addEventListener('click',
                onHandlerClick.bind(this));
            this._domElements.actionBtn.addEventListener('click',
                onActionExecute.bind(this));
            this._domElements.selectAllBtn.addEventListener('click',
                onSelectAllClick.bind(this));
            this._domElements.deselectAllBtn.addEventListener('click',
                this._onDeselectAllClick);
        }

        /**
         * MultiSelect class constructor. The class adds the multiselect
         * feature to the specified list.
         *
         * @constructor
         * @memberof helpers/multiselect
         * @public
         * @param {HTMLElement} listElement
         */
        function MultiSelect(listElement) {
            this._listElement = listElement;
            this._domElements = createDOMElements(listElement);
            this._onListClick = onListClick.bind(this);
            this._onDeselectAllClick = onDeselectAllClick.bind(this);
            this._selectCount = 0;
            this._listItems = this._listElement.getElementsByTagName('a');
            bindEvents.call(this);
        }

        /**
         * Displays helper elements of the selection process: the handler button
         * displaying number of selected items and the action button.
         *
         * @private
         */
        MultiSelect.prototype._modeShow = function modeShow() {
            var wrapper = this._domElements.wrapper;

            wrapper.classList.remove(OPEN_MULTISELECT_CLASS);
            wrapper.classList.add(SHOW_BTN_CLASS);
            this._refreshText();
        };

        /**
         * Hides helper elements of the selection process: the handler button
         * and the action button.
         *
         * @private
         */
        MultiSelect.prototype._modeHide = function modeHide() {
            var wrapper = this._domElements.wrapper;

            wrapper.classList.remove(OPEN_MULTISELECT_CLASS);
            wrapper.classList.remove(SHOW_BTN_CLASS);
            this._selectCount = 0;
            this._refreshText();
        };

        /**
         * Refreshes text of the handler button.
         *
         * @private
         */
        MultiSelect.prototype._refreshText = function refreshText() {
            this._domElements.handlerBtnText.innerText = this._selectCount;
        };

        /**
         * This method should be called when the selection process must be
         * turned off.
         *
         * The method removes elements of the selection process and unbinds
         * click event on the list causing selecting the list items.
         *
         * @public
         */
        MultiSelect.prototype.destroy = function destroy() {
            var wrapper = this._domElements.wrapper;

            this._onDeselectAllClick();
            wrapper.parentNode.removeChild(wrapper);
            this._listElement.removeEventListener('click', this._onListClick);
        };

        /**
         * Sets text on the action button.
         *
         * @public
         * @param {string} name
         */
        MultiSelect.prototype.setActionName = function setActionName(name) {
            this._domElements.actionBtnText.innerText = name;
        };

        /**
         * Sets the handler of the action button.
         * The handler will be executed when user clicks on the action button.
         *
         * @public
         * @param {function} handler
         */
        MultiSelect.prototype.setActionHandler =
            function setActionHandler(handler) {
                this._actionHandler = handler;
            };

        return MultiSelect;
    }
);
