/*
 * Copyright (c) 2015 Samsung Electronics Co., Ltd. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*global define*/

/**
 * DOM helper module.
 *
 * @module helpers/dom
 * @namespace helpers/dom
 */
define('helpers/dom', function dom() {
    'use strict';

    /**
     * Finds in the DOM tree the closest element with specified tag name
     * looking from the specified node towards the body element.
     *
     * @memberof helpers/dom
     * @public
     * @param {HTMLElement} element
     * @param {string} tagName
     * @returns {HTMLElement}
     */
    function findClosestElementByTag(element, tagName) {
        while (element.tagName !== tagName && element.tagName !== 'BODY') {
            element = element.parentNode;
        }

        return element.tagName === tagName ? element : null;
    }

    return {
        findClosestElementByTag: findClosestElementByTag
    };
});
