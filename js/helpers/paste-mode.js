/*
 * Copyright (c) 2015 Samsung Electronics Co., Ltd. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*global define, document*/

/**
 * Paste mode helper module.
 *
 * @module helpers/paste-mode
 * @namespace helpers/paste-mode
 */
define('helpers/paste-mode', function pasteModeModule() {
        'use strict';

        /**
         * Visible CSS class.
         *
         * It allows the application to decide whether the paste mode buttons
         * (confirm and cancel) should be visible or not.
         *
         * @memberof helpers/paste-mode
         * @private
         * @const {string}
         */
        var VISIBLE_CLASS = 'visible',

            /**
             * Reference to the cancel button.
             *
             * @memberof helpers/paste-mode
             * @private
             * @type {HTMLElement}
             */
            cancelBtn = null,

            /**
             * Reference to the confirm button.
             *
             * @memberof helpers/paste-mode
             * @private
             * @type {HTMLElement}
             */
            confirmBtn = null,

            /**
             * Reference to the wrapper element.
             *
             * @memberof helpers/paste-mode
             * @private
             * @type {HTMLElement}
             */
            wrapperEl = null;

        /**
         * Displays the paste buttons: confirm and cancel.
         *
         * @memberof helpers/paste-mode
         * @public
         */
        function show() {
            wrapperEl.classList.add(VISIBLE_CLASS);
        }

        /**
         * Hides the paste buttons.
         *
         * @memberof helpers/paste-mode
         * @public
         */
        function hide() {
            wrapperEl.classList.remove(VISIBLE_CLASS);
        }

        /**
         * Binds event listeners.
         *
         * @memberof helpers/paste-mode
         * @public
         * @param {function} onPasteConfirmClick Executed when the confirm
         * button is clicked.
         * @param {function} onPasteCancelClick Executed when the paste button
         * is clicked.
         */
        function bindEvents(onPasteConfirmClick, onPasteCancelClick) {
            confirmBtn.addEventListener('click', onPasteConfirmClick);
            cancelBtn.addEventListener('click', onPasteCancelClick);
        }

        /**
         * Initializes the module.
         *
         * @memberof helpers/paste-mode
         * @public
         */
        function init() {
            wrapperEl = document.getElementById('paste-mode-wrapper');
            confirmBtn = wrapperEl.querySelector('.paste-btn');
            cancelBtn = wrapperEl.querySelector('.cancel-btn');
        }

        return {
            init: init,
            bindEvents: bindEvents,
            show: show,
            hide: hide
        };
    }
);
