/*
 * Copyright (c) 2015 Samsung Electronics Co., Ltd. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*global define, document, tau*/

/**
 * Breadcrumb navigation list view module.
 *
 * @module views/breadcrumb
 * @requires {@link core/template}
 * @requires {@link helpers/dom}
 * @requires {@link models/state}
 * @namespace views/breadcrumb
 */
define('views/breadcrumb',
    ['core/template', 'helpers/dom', 'models/state'],
    function breadcrumb(template, dom, state) {
        'use strict';

        /**
         * Reference to the navigation list element.
         *
         * @memberof views/breadcrumb
         * @private
         * @type {HTMLElement}
         */
        var navigationList = null,

            /**
             * Reference to the page element.
             *
             * @memberof views/breadcrumb
             * @private
             * @type {HTMLElement}
             */
            page = null;

        /**
         * Returns an array containing objects needed by the HTML list template
         * generated using the specified path.
         *
         * Generated elements refer to directories of the given path
         * and every of them contains the following fields:
         * {string} name: name of directory
         * {string} path: full path of directory
         *
         * @memberof views/breadcrumb
         * @private
         * @param {string} path
         * @returns {object[]}
         */
        function createTemplateParams(path) {
            if (path.length === 0) {
                return [];
            }

            var pathElements = path.split('/'),
                i = 1,
                length = pathElements.length,
                currentPath = pathElements.shift(),
                currentName = currentPath,
                result = [];

            for (; i < length; i += 1) {
                result.push({
                    name: currentName,
                    path: currentPath
                });
                currentName = pathElements.shift();
                currentPath += '/' + currentName;
            }

            result.push({
                name: currentName,
                path: currentPath
            });

            return result;
        }

        /**
         * Reloads the navigation list.
         *
         * @memberof views/breadcrumb
         * @private
         */
        function reloadList() {
            navigationList.innerHTML = template.get(
                'navigation-list-content',
                {items: createTemplateParams(state.getCurrentPath())}
            );
        }

        /**
         * Handles pagebeforeshow event.
         *
         * @memberof views/breadcrumb
         * @private
         */
        function onPageBeforeShow() {
            reloadList();
        }

        /**
         * Handles click event on the navigation list element.
         *
         * @memberof views/breadcrumb
         * @private
         * @param {Event} ev
         */
        function onNavigationListClick(ev) {
            var anchor = dom.findClosestElementByTag(ev.target, 'A');

            if (anchor) {
                state.setCurrentPath(anchor.dataset.path);
                tau.back();
            }
        }

        /**
         * Registers event listeners.
         *
         * @memberof views/breadcrumb
         * @private
         */
        function bindEvents() {
            page.addEventListener('pagebeforeshow', onPageBeforeShow);
            navigationList.addEventListener('click', onNavigationListClick);
        }

        /**
         * Initializes module.
         *
         * @memberof views/breadcrumb
         * @public
         */
        function init() {
            page = document.getElementById('breadcrumb');
            navigationList = document.getElementById('navigation-list');
            bindEvents();
        }

        return {
            init: init
        };
    }
);
