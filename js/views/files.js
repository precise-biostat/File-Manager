/*
 * Copyright (c) 2015 Samsung Electronics Co., Ltd. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*global define, document, tau, console*/

/**
 * Files list view module.
 *
 * @module views/files
 * @requires {@link core/template}
 * @requires {@link models/state}
 * @requires {@link models/filesystem}
 * @requires {@link helpers/popup}
 * @requires {@link helpers/menu}
 * @requires {@link helpers/dom}
 * @requires {@link helpers/multiselect}
 * @requires {@link helpers/paste-mode}
 * @requires {@link helpers/capability}
 * @requires {@link helpers/page}
 * @namespace views/files
 */
// jshint maxstatements: 46
define('views/files', [
        'core/template',
        'models/state',
        'models/filesystem',
        'helpers/popup',
        'helpers/menu',
        'helpers/dom',
        'helpers/multiselect',
        'helpers/paste-mode',
        'helpers/capability',
        'helpers/page'
    ], function viewsFiles(require) {
        'use strict';

        /**
         * Template core module.
         *
         * @memberof views/files
         * @private
         * @type {Module}
         */
        var template = require('core/template'),

            /**
             * Application state module.
             *
             * @memberof views/files
             * @private
             * @type {Module}
             */
            state = require('models/state'),

            /**
             * Filesystem module.
             *
             * @memberof views/files
             * @private
             * @type {Module}
             */
            filesystem = require('models/filesystem'),

            /**
             * Popup helper module.
             *
             * @memberof views/files
             * @private
             * @type {Module}
             */
            popups = require('helpers/popup'),

            /**
             * Menu helper module.
             *
             * @memberof views/files
             * @private
             * @type {Module}
             */
            menu = require('helpers/menu'),

            /**
             * DOM helper module.
             *
             * @memberof views/files
             * @private
             * @type {Module}
             */
            dom = require('helpers/dom'),

            /**
             * MultiSelect constructor.
             *
             * @memberof views/files
             * @private
             * @type {MultiSelect}
             */
            MultiSelect = require('helpers/multiselect'),

            /**
             * Paste mode helper module.
             *
             * @memberof views/files
             * @private
             * @type {Module}
             */
            pasteHelper = require('helpers/paste-mode'),

            /**
             * Capability helper module.
             *
             * @memberof views/files
             * @private
             * @type {Module}
             */
            capability = require('helpers/capability'),

            /**
             * Page helper module.
             *
             * @memberof views/files
             * @private
             * @type {Module}
             */
            pageHelper = require('helpers/page'),

            /**
             * Reference to the page element.
             *
             * @memberof views/files
             * @private
             * @type {HTMLElement}
             */
            page = null,

            /**
             * Reference to the title element.
             *
             * @memberof views/files
             * @private
             * @type {HTMLElement}
             */
            title = null,

            /**
             * Reference to the file list.
             *
             * @memberof views/files
             * @private
             * @type {HTMLElement}
             */
            fileList = null,

            /**
             * Reference to the empty directory message element.
             *
             * @memberof views/files
             * @private
             * @type {HTMLElement}
             */
            emptyDirectoryMessage = null,

            /**
             * Reference to the MultiSelect object created on the file list if
             * the list is in the multiselect mode, null otherwise.
             *
             * @memberof views/files
             * @private
             * @type {MultiSelect}
             */
            multiselect = null,

            /**
             * Indicates whether keyboard is supported or not.
             *
             * @memberof views/files
             * @private
             * @type {boolean}
             */
            isKeyboardSupported = false,

            /**
             * List of files in the current directory.
             *
             * @memberof views/files
             * @private
             * @type {File[]}
             */
            files = [],

            /**
             * Executes a custom action on the path of the selected element.
             *
             * @memberof views/files
             * @private
             * @type {function}
             */
            selectedElementCustomAction = null;

        /**
         * Compares files specified as parameters in order to sort them.
         *
         * Comparator function for sorting files with the following roles:
         * 1. Always directories first
         * 2. Alphanumeric order
         *
         * @memberof views/files
         * @private
         * @param {File} file1
         * @param {File} file2
         * @returns {number}
         */
        function compareFiles(file1, file2) {
            var result = Number(file2.isDirectory) - Number(file1.isDirectory),
                name1 = null,
                name2 = null;

            if (result !== 0) {
                return result;
            }

            name1 = file1.name.toLowerCase();
            name2 = file2.name.toLowerCase();

            return name1 < name2 ? -1 : name1 === name2 ? 0 : 1;
        }

        /**
         * Reloads the list of files.
         *
         * @memberof views/files
         * @private
         */
        function reloadFileList() {
            pageHelper.resetScroll(page);
            fileList.innerHTML = template.get('file-list-content', {
                files: files
            });
        }

        /**
         * Returns full path of file/directory in the currently displayed
         * directory having the specified name.
         *
         * @memberof views/files
         * @private
         * @param {string} fileName
         * @returns {string}
         */
        function getNodePath(fileName) {
            return state.getCurrentPath() + filesystem.SEPARATOR + fileName;
        }

        /**
         * Opens the directory having the specified name.
         *
         * @memberof views/files
         * @private
         * @param {string} name
         */
        function openDirectory(name) {
            state.setCurrentPath(getNodePath(name));
            reloadPage();
        }

        /**
         * Opens the file having the specified name.
         *
         * @memberof views/files
         * @private
         * @param {string} name
         */
        function openFile(name) {
            filesystem.openFile(getNodePath(name)).catch(onError);
        }

        /**
         * Handles click event on the file list in the normal mode.
         * Opens the file or directory referred by the clicked item.
         *
         * @memberof views/files
         * @private
         * @param {Event} ev
         */
        function onListElementClick(ev) {
            var anchor = dom.findClosestElementByTag(ev.target, 'A'),
                name = null;

            if (anchor) {
                name = anchor.dataset.name;
                if (anchor.dataset.directory === 'true') {
                    openDirectory(name);
                } else if (state.getMode() === state.MODE_NORMAL) {
                    openFile(name);
                }
            }
        }

        /**
         * Handles click event on the file list in the select mode.
         *
         * @memberof views/files
         * @private
         * @param {Event} ev
         */
        function onListElementSelect(ev) {
            var anchor = dom.findClosestElementByTag(ev.target, 'A');

            if (anchor) {
                selectedElementCustomAction(getNodePath(anchor.dataset.name));
            }
        }

        /**
         * Handles click event on the file list element.
         *
         * @memberof views/files
         * @private
         * @param {Event} ev
         */
        function onFileListClick(ev) {
            switch (state.getMode()) {
                case state.MODE_MULTISELECT:
                    return;

                case state.MODE_SELECT:
                    onListElementSelect(ev);
                    return;

                case state.MODE_NORMAL:
                case state.MODE_PASTE:
                    onListElementClick(ev);
                    return;

                default:
                    console.error('Invalid mode of the app: ', state.getMode());
                    return;
            }
        }

        /**
         * Handles files from the current directory released by the model.
         * Displays these files on the list.
         *
         * @memberof views/files
         * @private
         * @param {File[]} result
         */
        function onGetFilesSuccess(result) {
            files = result.sort(compareFiles);
            emptyDirectoryMessage.classList.toggle('hidden', files.length > 0);
            reloadFileList();
            refreshMenu();
        }

        /**
         * Removes multiselect and paste buttons from the page if they exist.
         *
         * @memberof views/files
         * @private
         * @param {object} [params]
         */
        function destroyHelperUiElements(params) {
            if (multiselect) {
                multiselect.destroy();
                multiselect = null;
            }

            if (params && params.doNotHidePasteButtons) {
                return;
            }

            pasteHelper.hide();
        }

        /**
         * Sets the normal mode.
         *
         * @memberof views/files
         * @private
         * @param {object} [params]
         */
        function setNormalMode(params) {
            destroyHelperUiElements(params);
            title.innerText = state.getCurrentPath().split('/').pop();
            state.setMode(state.MODE_NORMAL);
        }

        /**
         * Opens multiselect mode.
         *
         * @memberof views/files
         * @private
         * @param {string} actionName Name of action that is supposed to be
         * invoked after files selection.
         * @param {function} actionHandler Action that is supposed to be invoked
         * after files selection.
         */
        function setMultiselectMode(actionName, actionHandler) {
            destroyHelperUiElements();
            state.setMode(state.MODE_MULTISELECT);
            multiselect = new MultiSelect(fileList);
            multiselect.setActionName(actionName);
            multiselect.setActionHandler(actionHandler);
            title.innerText = 'Select files';
        }

        /**
         * Sets the select mode.
         *
         * @memberof views/files
         * @private
         * @param {function} callback
         */
        function setSelectMode(callback) {
            destroyHelperUiElements();
            selectedElementCustomAction = callback;
            state.setMode(state.MODE_SELECT);
            title.innerText = 'Select a file';
        }

        /**
         * Performs go back action.
         *
         * @memberof views/files
         * @public
         */
        function goBack() {
            if (document.querySelector('.ui-popup-active')) {
                tau.closePopup();
            } else if (state.getMode() === state.MODE_SELECT ||
                state.getMode() === state.MODE_MULTISELECT) {
                setNormalMode();
            } else {
                state.setCurrentPath(
                    filesystem.getParentPath(state.getCurrentPath())
                );
                if (state.getCurrentPath() === '') {
                    tau.back();
                } else {
                    reloadPage();
                }
            }
        }

        /**
         * Handles get files error. Displays error message and goes back to
         * the previous location.
         *
         * @memberof views/files
         * @private
         * @param {Error} error
         */
        function onGetFilesError(error) {
            files = [];
            reloadFileList();
            refreshMenu();
            popups.showToast('Error occurred during getting files from ' +
                'the storage: ' + error.message, {onExit: goBack});
        }

        /**
         * Handles errors.
         *
         * @memberof views/files
         * @private
         * @param {Error} error
         */
        function onError(error) {
            popups.showToast('Error occurred: ' + error.message);
        }

        /**
         * Refreshes visibility of the menu items.
         *
         * @memberof views/files
         * @private
         */
        function refreshMenu() {
            var isFolderEmpty = files.length === 0;

            menu.setItemHidden('menu-rename',
                !isKeyboardSupported || isFolderEmpty);
            menu.setItemHidden('menu-delete', isFolderEmpty);
            menu.setItemHidden('menu-copy', isFolderEmpty);
            menu.setItemHidden('menu-cut', isFolderEmpty);
            menu.setItemHidden('menu-info', isFolderEmpty);
            menu.updateHiddenFilesItem(state.getHiddenFilesVisible());
        }

        /**
         * Reloads the page.
         * Obtains file list of the current directory and displays it on the
         * page.
         *
         * @memberof views/files
         * @private
         */
        function reloadPage() {
            title.innerText = state.getCurrentPath().split('/').pop();
            filesystem
                .getFiles(state.getCurrentPath(), state.getHiddenFilesVisible())
                .then(onGetFilesSuccess, onGetFilesError);
        }

        /**
         * Handles pagebeforeshow event.
         *
         * @memberof views/files
         * @private
         */
        function onPageShow() {
            reloadPage();
        }

        /**
         * Handles the folder name entered by user in the input popup.
         * Creates a folder with the given name.
         *
         * @memberof views/files
         * @private
         * @param {string} name
         */
        function onEnterFolderName(name) {
            filesystem.createDirectory(getNodePath(name))
                .then(reloadPage, onError);
        }

        /**
         * Handles click event on the create folder menu button.
         *
         * @memberof views/files
         * @private
         */
        function onCreateFolderMenuItemClicked() {
            setNormalMode({doNotHidePasteButtons: true});
            if (isKeyboardSupported) {
                popups.showInput('Enter folder name:', onEnterFolderName);
            } else {
                filesystem.generateName(
                    state.getCurrentPath() + filesystem.SEPARATOR + 'Folder'
                ).then(onEnterFolderName).catch(onError);
            }
        }

        /**
         * Handles the file name entered by user in the input popup.
         * Creates a file with the given name.
         *
         * @memberof views/files
         * @private
         * @param {string} name
         */
        function onEnterFileName(name) {
            filesystem.createFile(getNodePath(name)).then(reloadPage, onError);
        }

        /**
         * Handles click event on the create file menu button.
         *
         * @memberof views/files
         * @private
         */
        function onCreateFileMenuItemClicked() {
            setNormalMode();
            if (isKeyboardSupported) {
                popups.showInput('Enter file name:', onEnterFileName);
            } else {
                filesystem.generateName(
                    state.getCurrentPath() + filesystem.SEPARATOR + 'File'
                ).then(onEnterFileName).catch(onError);
            }
        }

        /**
         * Handles file/directory selected in renaming process.
         *
         * @memberof views/files
         * @private
         * @param {string} path
         */
        function onFileToRenameSelected(path) {
            setNormalMode();
            popups.showInput('Enter new name:', function onGetName(name) {
                filesystem.rename(path, name).then(reloadPage, onError);
            });
        }

        /**
         * Handles click event on the rename menu button.
         *
         * @memberof views/files
         * @private
         */
        function onRenameMenuItemClicked() {
            setSelectMode(onFileToRenameSelected);
        }

        /**
         * Handles click event on the delete selected files action button.
         *
         * @memberof views/files
         * @private
         * @param {HTMLElement[]} listItems
         */
        function onItemsToDeleteChoose(listItems) {
            var paths = listItemsToPaths(listItems);

            setNormalMode();

            popups.showConfirmPopup(
                'Do you really want to remove ' + paths.length + ' file(s)?',
                function onConfirmed() {
                    filesystem.deleteFiles(paths).then(function onSuccess() {
                        popups.showToast(
                            'File(s) deleted successfully',
                            {autoClose: true}
                        );
                        reloadPage();
                    }, onError);
                }
            );
        }

        /**
         * Handles click event on the delete menu button.
         *
         * @memberof views/files
         * @private
         */
        function onDeleteMenuItemClicked() {
            setMultiselectMode('Delete', onItemsToDeleteChoose);
        }

        /**
         * Returns an array of paths of files represented by the specified
         * elements.
         *
         * @memberof views/files
         * @private
         * @param {HTMLElement[]} listItems
         * @returns {string[]}
         */
        function listItemsToPaths(listItems) {
            var i = 0,
                length = listItems.length,
                result = [];

            for (; i < length; i += 1) {
                result.push(getNodePath(listItems[i].dataset.name));
            }

            return result;
        }

        /**
         * Handles the click event on the paste button.
         * Starts cut or copy operation.
         *
         * @memberof views/files
         * @private
         */
        function onPasteConfirmClick() {
            var actionName = state.getClipboardActionName();

            setNormalMode();

            if (actionName === state.ACTION_CUT) {
                moveFiles();
            } else if (actionName === state.ACTION_COPY) {
                copyFiles();
            } else {
                console.error('Unsupported clipboard action name', actionName);
            }
        }

        /**
         * Handles the click event on the cancel button.
         * Interrupts cut or copy operation.
         *
         * @memberof views/files
         * @private
         */
        function onPasteCancelClick() {
            setNormalMode();
        }

        /**
         * Sets paste mode.
         * Saves files represented by the specified fileList elements to
         * clipboard and displays paste and cancel buttons.
         *
         * @memberof views/files
         * @private
         * @param {HTMLElement[]} listItems
         * @param {string} clipboardActionName
         */
        function setPasteMode(listItems, clipboardActionName) {
            var paths = listItemsToPaths(listItems);

            state.setClipboardFiles(paths);
            state.setClipboardActionName(clipboardActionName);
            state.setMode(state.MODE_PASTE);
            destroyHelperUiElements();
            pasteHelper.show();
        }

        /**
         * Handles selected fileList items during the copy operation.
         *
         * @memberof views/files
         * @private
         * @param {HTMLElement[]} listItems
         */
        function onItemsToCopyChoose(listItems) {
            setPasteMode(listItems, 'Copy');
        }

        /**
         * Handles selected fileList items during the cut operation.
         *
         * @memberof views/files
         * @private
         * @param {HTMLElement[]} listItems
         */
        function onItemsToCutChoose(listItems) {
            setPasteMode(listItems, 'Cut');
        }

        /**
         * Handles the click event on the copy menu item.
         * Starts the copy operation.
         *
         * @memberof views/files
         * @private
         */
        function onCopyMenuItemClicked() {
            setMultiselectMode(state.ACTION_COPY, onItemsToCopyChoose);
        }

        /**
         * Handles the click event on the cut menu item.
         * Starts the cut operation.
         *
         * @memberof views/files
         * @private
         */
        function onCutMenuItemClicked() {
            setMultiselectMode(state.ACTION_CUT, onItemsToCutChoose);
        }

        /**
         * Moves files from clipboard to the current directory.
         *
         * @memberof views/files
         * @private
         */
        function moveFiles() {
            filesystem.moveFiles(state.getClipboardFiles(),
                state.getCurrentPath())
                .then(function onSuccess() {
                    popups.showToast(
                        'File(s) moved successfully',
                        {autoClose: true}
                    );
                }, onError)
                .then(function onComplete() {
                    state.clearClipboard();
                    reloadPage();
                });
        }

        /**
         * Copies files from clipboard to the current directory.
         *
         * @memberof views/files
         * @private
         */
        function copyFiles() {
            filesystem.copyFiles(state.getClipboardFiles(),
                state.getCurrentPath())
                .then(function onSuccess() {
                    popups.showToast(
                        'File(s) copied successfully',
                        {autoClose: true}
                    );
                }, onError)
                .then(function onComplete() {
                    state.clearClipboard();
                    reloadPage();
                });
        }

        /**
         * Returns a file from the current directory that has path equal to the
         * specified one.
         *
         * @memberof views/files
         * @private
         * @param {string} filePath
         * @returns {File}
         */
        function getFileByPath(filePath) {
            var i = 0,
                length = files.length,
                file = null;

            for (; i < length; i += 1) {
                file = files[i];
                if (file.fullPath === filePath) {
                    return file;
                }
            }

            return null;
        }

        /**
         * Receives the path of the file/folder selected to show its info.
         *
         * @memberof views/files
         * @private
         * @param {string} path
         */
        function onFileToShowInfoSelected(path) {
            var file = getFileByPath(path);

            state.setSelectedFile(file);
            setNormalMode();
            tau.changePage('#file-info');
        }

        /**
         * Handles click event on the info menu button.
         *
         * @memberof views/files
         * @private
         */
        function onInfoMenuItemClicked() {
            setSelectMode(onFileToShowInfoSelected);
        }

        /**
         * Handles click event on the breadcrumb navigation menu button.
         *
         * @memberof views/files
         * @private
         */
        function onBreadcrumbMenuItemClicked() {
            tau.changePage('#breadcrumb');
        }

        /**
         * Handles click event on the hidden files menu button.
         *
         * @memberof views/files
         * @private
         */
        function onHiddenFilesMenuItemClicked() {
            state.setHiddenFilesVisible(!state.getHiddenFilesVisible());
            reloadPage();
        }

        /**
         * Binds page events.
         *
         * @memberof views/files
         * @private
         */
        function bindEvents() {
            page.addEventListener('pagebeforeshow', onPageShow);
            fileList.addEventListener('click', onFileListClick);

            menu.bindMenuClickEventListener('menu-delete',
                onDeleteMenuItemClicked);

            menu.bindMenuClickEventListener('menu-create-folder',
                onCreateFolderMenuItemClicked);

            menu.bindMenuClickEventListener('menu-create-file',
                onCreateFileMenuItemClicked);

            menu.bindMenuClickEventListener('menu-rename',
                onRenameMenuItemClicked);

            menu.bindMenuClickEventListener('menu-copy',
                onCopyMenuItemClicked);

            menu.bindMenuClickEventListener('menu-cut',
                onCutMenuItemClicked);

            menu.bindMenuClickEventListener('menu-info',
                onInfoMenuItemClicked);

            menu.bindMenuClickEventListener('menu-breadcrumb',
                onBreadcrumbMenuItemClicked);

            menu.bindMenuClickEventListener('menu-hidden-files',
                onHiddenFilesMenuItemClicked);

            pasteHelper.bindEvents(onPasteConfirmClick, onPasteCancelClick);
        }

        /**
         * Initializes module.
         *
         * @memberof views/files
         * @public
         */
        function init() {
            page = document.getElementById('files');
            fileList = document.getElementById('file-list');
            title = page.querySelector('header h2');
            emptyDirectoryMessage = document.getElementById('empty-dir-msg');
            isKeyboardSupported = capability.isKeyboardAvailable();
            menu.initializeMenu(page, 'open-files-menu');
            bindEvents();
        }

        return {
            init: init,
            goBack: goBack
        };
    }
);
