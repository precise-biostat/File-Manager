/*
 * Copyright (c) 2015 Samsung Electronics Co., Ltd. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*global define*/

/**
 * Application state module.
 *
 * @module models/state
 * @namespace models/state
 */
define('models/state', function state() {
    'use strict';

    /**
     * Copy action key.
     *
     * @memberof models/state
     * @public
     * @const {string}
     */
    var ACTION_COPY = 'Copy',

        /**
         * Cut action key.
         *
         * @memberof models/state
         * @public
         * @const {string}
         */
        ACTION_CUT = 'Cut',

        /**
         * Normal mode.
         * In this mode the user can open files or folders by clicking on the
         * file list.
         *
         * @memberof models/state
         * @public
         * @const {string}
         */
        MODE_NORMAL = 'normal',

        /**
         * Multiselect mode.
         * In this mode the user selects one or more files from the file list.
         *
         * @memberof models/state
         * @public
         * @const {string}
         */
        MODE_MULTISELECT = 'multiselect',

        /**
         * Select mode.
         * In this mode the user selects one file from the file list to rename
         * it or display its properties.
         *
         * @memberof models/state
         * @public
         * @const {string}
         */
        MODE_SELECT = 'select',

        /**
         * Paste mode.
         * In this mode the user pastes the copied/cut files into the currently
         * displayed directory.
         *
         * @memberof models/state
         * @public
         * @const {string}
         */
        MODE_PASTE = 'paste',

        /**
         * Path of the currently displayed folder.
         *
         * @memberof models/state
         * @private
         * @type {string}
         */
        currentPath = '',

        /**
         * List of file paths that are stored temporarily to copy/move.
         *
         * @memberof models/state
         * @private
         * @type {string[]}
         */
        clipboard = [],

        /**
         * The name of action that supposed to be executed on the stored files.
         *
         * @memberof models/state
         * @private
         * @type {string}
         */
        actionName = '',

        /**
         * Selected file.
         *
         * @memberof models/state
         * @private
         * @type {File}
         */
        selectedFile = null,

        /**
         * Current file list mode.
         *
         * @memberof models/state
         * @private
         * @type {string}
         */
        mode = MODE_NORMAL,

        /**
         * Flag indicating whether hidden files are displayed or not.
         *
         * @memberof models/state
         * @private
         * @type {boolean}
         */
        hiddenFilesVisible = false;

    /**
     * Sets the currently displayed path.
     *
     * @memberof models/state
     * @public
     * @param {string} path
     */
    function setCurrentPath(path) {
        currentPath = path;
    }

    /**
     * Returns the currently displayed path.
     *
     * @memberof models/state
     * @public
     * @returns {string}
     */
    function getCurrentPath() {
        return currentPath;
    }

    /**
     * Stores the specified files in the clipboard.
     *
     * @memberof models/state
     * @public
     * @param {string[]} filePaths
     */
    function setClipboardFiles(filePaths) {
        clipboard = filePaths;
    }

    /**
     * Sets the name of action that supposed to be executed on the stored
     * files.
     *
     * @memberof models/state
     * @public
     * @param {string} name
     */
    function setClipboardActionName(name) {
        actionName = name;
    }

    /**
     * Returns the name of action that supposed to be executed on the stored
     * files.
     *
     * @memberof models/state
     * @public
     * @returns {string}
     */
    function getClipboardActionName() {
        return actionName;
    }

    /**
     * Returns list of file paths stored in the clipboard.
     *
     * @memberof models/state
     * @public
     * @returns {string[]}
     */
    function getClipboardFiles() {
        return clipboard;
    }

    /**
     * Clears the clipboard.
     *
     * @memberof models/state
     * @public
     */
    function clearClipboard() {
        clipboard = [];
    }

    /**
     * Sets the selected file.
     *
     * @memberof models/state
     * @public
     * @param {File} file
     */
    function setSelectedFile(file) {
        selectedFile = file;
    }

    /**
     * Returns the selected file.
     *
     * @memberof models/state
     * @public
     * @returns {File}
     */
    function getSelectedFile() {
        return selectedFile;
    }

    /**
     * Sets the file list mode.
     *
     * @memberof models/state
     * @public
     * @param {string} newMode
     */
    function setMode(newMode) {
        mode = newMode;
    }

    /**
     * Returns the file list mode.
     *
     * @memberof models/state
     * @public
     * @returns {string}
     */
    function getMode() {
        return mode;
    }

    /**
     * Sets the hidden files visibility flag.
     *
     * @memberof models/state
     * @public
     * @param {boolean} visible
     */
    function setHiddenFilesVisible(visible) {
        hiddenFilesVisible = visible;
    }

    /**
     * Returns the hidden files visibility flag.
     *
     * @memberof models/state
     * @public
     * @returns {boolean}
     */
    function getHiddenFilesVisible() {
        return hiddenFilesVisible;
    }

    return {
        setCurrentPath: setCurrentPath,
        getCurrentPath: getCurrentPath,
        setClipboardFiles: setClipboardFiles,
        getClipboardFiles: getClipboardFiles,
        setClipboardActionName: setClipboardActionName,
        getClipboardActionName: getClipboardActionName,
        clearClipboard: clearClipboard,
        setSelectedFile: setSelectedFile,
        getSelectedFile: getSelectedFile,
        setMode: setMode,
        getMode: getMode,
        setHiddenFilesVisible: setHiddenFilesVisible,
        getHiddenFilesVisible: getHiddenFilesVisible,

        MODE_NORMAL: MODE_NORMAL,
        MODE_MULTISELECT: MODE_MULTISELECT,
        MODE_SELECT: MODE_SELECT,
        MODE_PASTE: MODE_PASTE,

        ACTION_COPY: ACTION_COPY,
        ACTION_CUT: ACTION_CUT
    };
});
