{{?foundInternals}}
<li class="ui-listview-divider">Internal</li>
{{/foundInternals}}
{{#internal}}
<li><a href="#" data-name="{{this.label}}">
    <img src="images/create-folder.png"/>
    {{this.label}}
</a></li>
{{/internal}}
{{?foundExternals}}
<li class="ui-listview-divider">External</li>
{{/foundExternals}}
{{#external}}
<li><a href="#" data-name="{{this.label}}">
    <img src="images/create-folder.png"/>
    {{this.label}}
</a></li>
{{/external}}